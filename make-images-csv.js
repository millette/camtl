'use strict'

// core
const path = require('path')

// npm
const glob = require('glob')

// const f = glob.sync('/home/millette/camtl/http/www1.ville.montreal.qc.ca/Circulation-Cameras/**/content') // GEN41.jpeg/

const f = glob.sync('http/www1.ville.montreal.qc.ca/Circulation-Cameras/GEN*') // GEN41.jpeg/

// console.log(f.length)

const out = {}

const g = f.map((i) => {
  return {
    ts: glob.sync(i + '/*/content').map((x) => x.split('/')[4]),
    cam: path.basename(i, '.jpeg').slice(3)
  }
})

g.forEach((i) => {
  out[i.cam] = i.ts
})

console.log(JSON.stringify(out, null, ' '))

