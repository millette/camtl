'use strict'

// core
const fs = require('fs')
const path = require('path')

// specific
const parseUrl = require('url').parse
const http = require('http')
const httpRequest = http.request
const keepAliveAgent = http.Agent({ keepAlive: true, maxSockets: 5 })

// npm
const mkdirp = require('mkdirp')
const pick = require('lodash.pick')

// process command line arguments
const verbose = process.argv.indexOf('--verbose') !== -1
// const getImages = process.argv.indexOf('--get-images') !== -1
const percentOf304 = process.argv.indexOf('--ratio')
let aim
if (percentOf304 !== -1) { aim = parseInt(process.argv[percentOf304 + 1]) }
if (!aim || aim > 100 || aim < 1) { aim = 0.04 } else { aim /= 100 }

// rest
let delayMinutes = 5

const fsClean = function (filename) {
  return path.normalize(filename)
    .replace(/(\d{1,2}):(\d{1,2}):(\d{1,2})/g, '$1h$2m$3')
    .replace(/(\d{1,2}):(\d{1,2})/g, '$1h$2m')
    .replace(/:/g, '')
}

const fsCleanNow = function (date) {
  if (date) { return fsClean(date.toISOString()) }
  return fsClean(new Date().toISOString())
}

const featuresArray = function (b) {
  return new Promise((resolve, reject) => {
    if (b) {
      try { resolve(JSON.parse(b).features) } catch (e) { reject(e) }
    } else { resolve([]) }
  })
}

const urlsHeaders = {
  'http://ville.montreal.qc.ca/circulation/sites/ville.montreal.qc.ca.circulation/files/cameras-de-circulation.json': {
    etag: null
  }
}

const httpRevs = function (u, method) {
  return new Promise((resolve, reject) => {
    const nopFn = function () {}
    const camsFilename = path.join(fsClean(u), fsCleanNow())

    mkdirp(camsFilename, () => {
      const parent = path.join(camsFilename, '..', 'headers.json')
      const parentContent = path.join(camsFilename, '..', 'content')

      fs.readFile(parent, (errRFP, body) => {
        const u2 = parseUrl(u)
        u2.method = (method || 'get').toUpperCase()

        if (!errRFP) {
          try {
            const headers = JSON.parse(body).headers
            if (headers && headers.etag) { u2.headers = { 'If-None-Match': headers.etag } }
          } catch (e) { reject(e) }
        }

        u2.agent = keepAliveAgent
        httpRequest(u2, (res) => {
          let cnt = ''
          // standard-format insists on breaking {} over 2 lines here
          res.on('data', (chunk) => {
            cnt += chunk
          })
          // standard-format insists on breaking {} over 2 lines here
          res.on('end', () => {
            if (res.statusCode === 200) { resolve(cnt) }
          })

          if (verbose) { console.log(res.statusCode, u) }
          urlsHeaders[u] = pick(res.headers, 'etag')
          urlsHeaders[u].statusCode = res.statusCode

          switch (res.statusCode) {
            case 200:
              if (res.headers['content-length']) {
                urlsHeaders[u]['content-length'] = parseInt(res.headers['content-length'], 10)
              }
              if (u2.method === 'GET') {
                res.pipe(fs.createWriteStream(path.join(camsFilename, 'content')))
                fs.unlink(parentContent, () => {
                  fs.symlink(path.join(path.basename(camsFilename), 'content'), parentContent, nopFn)
                })
              }
              break

            case 304:
              // standard-format insists on breaking {} over 2 lines here
              fs.readFile(parentContent, (err, data) => {
                resolve(err ? null : data)
              })
              return

            case 404:
              urlsHeaders[u]['content-length'] = 0
              resolve()
              break
          }

          fs.writeFile(path.join(camsFilename, 'headers.json'),
            JSON.stringify(pick(res, [ 'headers', 'statusCode' ]), null, ' ')
          )

          fs.unlink(parent, () => {
            fs.symlink(path.join(path.basename(camsFilename), 'headers.json'), parent, nopFn)
          })
        })
          // http server not found, not responding, wrong url?
          // could also be a connection reset
          .on('error', (err) => reject(err))
          .end()
      })
    })
  })
}

const dynamicDelay = function (statusCodes) {
  let tot = statusCodes[404] + statusCodes[304] + statusCodes[200]
  let ratio = statusCodes[304] / tot
  let correction = 1

  if (ratio > aim + 0.002) {
    correction = 1.05
  } else if (ratio < aim - 0.002) {
    correction = 0.98
  } else { return delayMinutes }
  return Math.max(Math.min(delayMinutes * correction, 8), 0.25)
}

// 1 out of 6
const pickme = () => Math.random() < 0.17 ? 'get' : 'head'

/*
const pickme = function () {
  const m = parseInt(Date.now() / 1000) % 3600 / 60
  return m < 5 || (m > 30 && m < 35) ? 'get' : 'head'
}
*/

const fn = function fn () {
  httpRevs(Object.keys(urlsHeaders)[0])
    .then(featuresArray)
    .then((c) => Promise.all(c.map((feature) => httpRevs(
//      feature.properties['url-image-en-direct'], getImages ? 'get' : 'head'
      feature.properties['url-image-en-direct'],
      pickme()
    ))))
    .then((g) => {
      let r
      let tot = 0
      const codes = { 200: 0, 304: 0, 404: 0 }
      const sizes = {}
      for (r in urlsHeaders) {
        ++tot
        ++codes[urlsHeaders[r].statusCode]
        if (urlsHeaders[r]['content-length'] < 6000) {
          let t = path.basename(r)
          if (sizes[urlsHeaders[r]['content-length']]) {
            sizes[urlsHeaders[r]['content-length']].push(t)
          } else { sizes[urlsHeaders[r]['content-length']] = [t] }
        }
      }

      let bads = 0
      for (r in sizes) { bads += Object.keys(sizes[r]).length }
      console.log(
        fsCleanNow().split('.')[0], (Math.round(delayMinutes * 100) / 100),
        tot, 'bad', bads, (Math.round(1000 * bads / codes[200]) / 10) + '%', codes
      )
      if (verbose) { console.log('sizes', sizes) }
      delayMinutes = dynamicDelay(codes)
      if (delayMinutes < 10 && delayMinutes > 0.1) {
        setTimeout(fn, delayMinutes * 60000)
      }
    })
    // standard-format insists on breaking {} over 2 lines here
    .catch((e) => {
      console.log('CAUGHT', e)
    })
}

fn()
