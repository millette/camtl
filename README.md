# camtl

## Installation
```bash
$ git clone [...]
$ npm install
$ cd .git/hooks/
$ ln -s ../../pre-commit
$ cd -
```

## Scripts disponibles
```bash
$ npm start         # démarrer le script principal, comme "node index"
$ npm run reformat  # reformatter index.js selon JS Standard
$ npm run lint      # vérifier index.js avec JS Standard
```

## Plateformes
Testé sur Jessie Debian GNU/Linux, curieux de savoir si ça fonctionne sur Windows vu l'usage de *symlinks*.

## Démarrage
```npm start``` lance le script principal, qui va créer le sous-répertoire ```http/``` (ou ```https/```) suivi de sous-répertoires pour les domaines puis de tous les sous-répertoires nécessaires pour chaque URL visité.

Chaque URL aura son propre sous-répertoire contenant des sous-répertoires pour chacun des accès (HTTP GET/HEAD). Ces derniers contiennent les fichiers ```content``` et ```headers.json```.

Le sous-répertoire de chaque URL contiendra aussi des liens symboliques vers les plus récents fichiers ```content``` et ```headers.json```.

### Arguments

```bash
$ node index --arg1 --arg2 ...
```

Remplacer ```node``` par ```nodejs``` sur certaines plateformes.

#### --verbose
Afficher le progrès et la liste des caméras hors d'usage.

#### --get-images
Par défaut, on ne télécharge pas les images. La méthode HTTP HEAD est utilisée pour obtenir l'info sur chaque caméra. Passer l'argument ```--get-images``` pour utiliser la méthode HTTP GET et télécharger les images.

#### --ratio 1-100
On attends un certain temps entre chaque itération. Ce délai s'adapte pour obtenir un ratio 50% entre les images en cache (304) et le total des images.

```bash
$ node index            # délai pour avoir un ratio de 4% de 304 (cache)
$ node index --ratio 50 # délai pour avoir un ratio de 50% de 304 (cache)
$ node index --ratio 80 # délai (plus court) pour avoir un ratio de 80% de 304 (cache)
$ node index --ratio 10 # délai (plus long) pour avoir un ratio de 10% de 304 (cache)
```


### Exemple de la structure de répertoires créés
```bash
$ tree http --noreport -r -t -n --charset UTF-8
http
└── ville.montreal.qc.ca
    └── circulation
        └── sites
            └── ville.montreal.qc.ca.circulation
                └── files
                    └── cameras-de-circulation.json
                        ├── headers.json -> 2016-02-21T19h33m07.821Z/headers.json
                        ├── 2016-02-21T19h33m07.821Z
                        │   └── headers.json
                        ├── 2016-02-21T19h32m56.169Z
                        │   └── headers.json
                        ├── 2016-02-21T19h31m09.804Z
                        │   └── headers.json
                        ├── content -> 2016-02-21T19h30m52.236Z/content
                        ├── 2016-02-21T19h30m52.236Z
                        │   ├── headers.json
                        │   └── content
                        ├── 2016-02-21T10h39m53.517Z
                        │   └── headers.json
                        ├── 2016-02-21T10h34m57.093Z
                        │   └── headers.json
                        └── 2016-02-21T10h23m56.591Z
                            ├── content
                            └── headers.json
```

## Dépendances
Développé avec ```node 4.3.1``` et ```npm 3.7.3``` en JavaScript moderne ([ECMAScript 6, aka ES2015](https://github.com/lukehoban/es6features/blob/master/README.md)), mais sans ```babel``` ou autre *transpiler*.

### Runtime
* ```mkdirp```
* ```lodash.pick```

### Dévelopement
* ```standard``` (lint)
* ```standard-format``` (reformatter)
* ```snazzy``` (embellir la sortie de ```standard```)

## Voir aussi
* [trafikomtl](http://pascalrobichaud.org/trafikomtl/) par Pascal Robichaud ([@DO101Mtl sur Twitter](https://twitter.com/DO101Mtl))
* [travaux bus stm](http://stm.waglo.com/) par [Robin Millette](http://robin.millette.info/)
