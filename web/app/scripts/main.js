/* global _, Dygraph */
(function () {
  'use strict'

  window.fetch('/cam-images.json')
  .then(function (response) {
    return response.json()
  }).then(function (pops) {
    const body = document.getElementsByTagName('body')[0]
    const pEl = document.createElement('p')
    const dataEl = document.createElement('a')
    const imgEl = document.getElementById('camimg')

    const parseLocation = function (search) {
      let s = {}
      window.location[search ? 'search' : 'hash'].slice(1).split('&').forEach((f) => {
        const p = f.split('=')
        s[p[0]] = p[1]
      })
      return s
    }

    const full = !parseLocation().embed
    let num = parseInt(parseLocation().cam, 10) || 4

    const pickTime = (ts) => pops[num].find((t) => new Date(t.replace('h', ':').replace('m', ':')).getTime() > ts)

    const highlight = function (event, x, points, row, seriesName) {
      const pt = pickTime(x)
      if (pt) { imgEl.src = '/Circulation-Cameras/GEN' + num + '.jpeg/' + pt + '/content' }
    }

    const g = new Dygraph(
      document.getElementById('graphdiv'),
      '/csv/GEN' + num + '.csv',
      {
        title: 'Caméra ' + num,
        labels: ['x', 'filesize'],
        axes: { x: { pixelsPerLabel: 40 } },
        showRoller: true,
        rollPeriod: 8,
        highlightCallback: _.debounce(highlight, 150)
      }
    )

    if (full) {
      const selEl = document.createElement('select')
      selEl.addEventListener('change', (ev) => {
        num = ev.target.value
        g.updateOptions({
          title: 'Caméra ' + num,
          file: '/csv/GEN' + num + '.csv'
        })
        window.location.hash = '#cam=' + num
        imgEl.src = 'http://www1.ville.montreal.qc.ca/Circulation-Cameras/GEN' + num + '.jpeg'
        dataEl.download = 'GEN' + num + '.csv'
        dataEl.href = '/csv/' + dataEl.download
      })
      selEl.innerHTML = Object.keys(pops).map((m) => '<option>' + m + '</option>').join('')
      body.appendChild(selEl)

      dataEl.innerHTML = 'données csv'
      dataEl.download = 'GEN' + num + '.csv'
      dataEl.href = '/csv/' + dataEl.download
      pEl.appendChild(dataEl)
      body.appendChild(pEl)
    } else {
      document.getElementById('morestuff').style.display = 'none'
    }
    imgEl.src = 'http://www1.ville.montreal.qc.ca/Circulation-Cameras/GEN' + num + '.jpeg'
    return 'ok'
  }).catch(function (ex) {
    console.log('parsing failed', ex)
  })
}())
