#!/bin/sh

echo step 1: make-images-csv
node make-images-csv >web/app/cam-images.json

echo step 2: rsync images
rsync -aPSzm --exclude=headers.json http/www1.ville.montreal.qc.ca/Circulation-Cameras cams.waglo.com:/var/local/www/cams.waglo.com/public/

echo step 3: make-csv
node make-csv

echo step 4: tarball
rm csv/csv-cams-mtl.tar.gz
tar czf csv/csv-cams-mtl.tar.gz csv/GEN*.csv

echo step 5: rsync data files (csv and json)
rsync -aPSz csv web/app/cam-images.json cams.waglo.com:/var/local/www/cams.waglo.com/public/

