'use strict'

// core
const fs = require('fs')
const path = require('path')

// npm
const csv = require('csv')
// const Error = require('errno-codes')

const camPaths = [__dirname, 'http/www1.ville.montreal.qc.ca/Circulation-Cameras/'].join('/')
const makeCsvFilename = (fn) => [__dirname, 'csv', path.basename(fn, '.jpeg') + '.csv'].join('/')

const parsed = function (lef, err, data) {
  if (err && err.code !== 'ENOENT') { return console.error(lef, err) }
  if (!data) { data = [] }
  const lastTime = data.length ? data[data.length - 1][0] : ''
  console.log(lef[0], lastTime)
  const mine = lef[2]
    .filter((f2) => f2 > lastTime)
    .map((f2) => {
      try {
        const x = require(camPaths + lef[0] + '/' + f2 + '/headers.json')
        if (x.statusCode === 200) {
          return x.headers && x.headers['content-length'] > 5999 ? x : { statusCode: -3 }
        } else { return { statusCode: -2 } }
      } catch (e) {
        return { statusCode: -1 }
      }
    })
    .filter((f2) => f2.statusCode === 200)
    .map((f2) => [new Date(f2.headers['last-modified']).toISOString().slice(0, -5) + 'Z', parseInt(f2.headers['content-length'], 10)])

  fs.appendFile(lef[1],
    mine.map((d) => d.join()).join('\n') + '\n',
    'utf8',
    (err9) => err9 ? console.error('err9', err9) : null
  )
}

const updateCsv = function (f, files) {
  const fn = makeCsvFilename(f)
  fs.createReadStream(fn)
    .on('error', parsed.bind(this, [f, fn, files]))
    .pipe(csv.parse(parsed.bind(this, [f, fn, files])))
}

const woot = function (camFile) {
  fs.readdir(camPaths + camFile, (err, timeFiles) => {
    if (err) { return console.error(err) }
    updateCsv(camFile, timeFiles)
  })
}

fs.readdir(camPaths, (err, files) => {
  if (err) { return console.error(err) }
  files
    // .slice(260, 325)
    .forEach(woot)
})
