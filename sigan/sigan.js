'use strict'

// core
const fs = require('fs')

// npm
const fftJS = require('fft-js')
const dft = fftJS.dft
// const fft = fftJS.fft
// const fftUtil = fftJS.util
const csv = require('csv')

const parsed = function (err, data) {
  if (err && err.code !== 'ENOENT') { return console.error(err) }
  if (!data) { data = [] }

  const sig = data.map((d) => d[1])
  const p2 = dft(sig)
  console.log('dft', p2)

/*
  const p = fft(sig)
  console.log('fft', p)

  const frequencies = fftUtil.fftFreq(p, 120) // Sample rate and coef is just used for length, and frequency step
  const  magnitudes = fftUtil.fftMag(p)

  const both = frequencies.map((f, ix) => {
    return {frequency: f, magnitude: magnitudes[ix]}
  })

  console.log('both', both)
*/
}

fs.createReadStream([__dirname, '/GEN204-last-2048.csv'].join('/'))
  .on('error', parsed)
  .pipe(csv.parse(parsed))
